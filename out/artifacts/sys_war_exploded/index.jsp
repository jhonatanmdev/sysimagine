<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>SysImagine</title>
    <meta charset="utf-8">
      <link rel="stylesheet" href="node_modules/bootstrap/compiler/bootstrap.css">
  </head>
  <body>
       <!-- terá dois níveis, cliente e administrador serão redirecionados automaticamente -->
      <section class="container col-md-6 col-lg-3 my-5">  
        <h1>SysImagine</h1>
        <form action="ServletLogin" method="post" >
          <div class="form-group ">
            <label for="login">Usuário</label>
            <input type="text" class="form-control" name="login" id="login" aria-describedby="login" placeholder="Usuário">
          </div>
          <div class="form-group">
            <label for="senha">Senha</label>
            <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha">
          </div>
          <input type="submit" class="btn btn-primary float-right" value="Enviar">
          <div class="text-danger float-left" >
            <p>${error}</p>
          </div>
        </form>
      </section>
  </body>
</html>