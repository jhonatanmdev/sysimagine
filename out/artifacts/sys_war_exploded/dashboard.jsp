<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="header.jsp"/>
    <section class="container">
        <div class="row my-2">
            <div class="col-md-4 my-1">
                <div class="card">
                    <div class="card-body bg-primary">
                        <h4 class="mb-2">Vendas
                        <i class="fas fa-chart-line float-right"></i>
                        </h4>
                        <h2 class="mb-3">R$ 3.600</h2>
                        <h6 class="card-text">Lucro no mês atual</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-1">
                <div class="card">
                    <div class="card-body bg-warning">
                        <h4 class="mb-2">Pedidos
                        <i class="fa fa-handshake float-right"></i>
                        </h4>
                        <h2 class="mb-3">43</h2>
                        <h6 class="card-text">Pedidos no mês atual</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-1">
                <div class="card">
                    <div class="card-body bg-success">
                        <h4 class="mb-2">Clientes
                        <i class="fa fa-users float-right"></i>
                        </h4>
                        <h2 class="mb-3">32</h2>
                        <h6 class="card-text">Clientes Cadastrados</h6>
                    </div>
                </div>
            </div>
        </div><!-- fim row -->
        <div class="row">
            <div class="col-md-6 my-1">
                <div class="card ">
                    <div class="card-header alert-warning">
                        <h5 class="card-header-text">Próximos pedidos</h5>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-uppercase">
                                        <th>ID</th>
                                        <th>Serviço</th>
                                        <th>Data de Venc.</th>
                                        <th>Cliente</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td>1</td>
                                    <td>2500u CV 4x0</td>
                                    <td>03/10/2018</td>
                                    <td>Maria</td>
                                    <td><span class="badge badge-success">Entregue</span></td>
                                    </tr>
                                    <tr class="my-1">
                                    <td>2</td>
                                    <td>5000 Pan. 4x0 90g</td>
                                    <td>29/10/2018</td>
                                    <td>Fulana</td>
                                    <td><span class="badge badge-warning">Atraso</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6 my-1">
                <div class="card">
                    <div class="card-header alert-success">
                        <h5 class="card-header-text">Clientes cadastrados</h5>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-uppercase">
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Data de Cad.</th>
                                        <th>CPF</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td>1</td>
                                    <td>Maria Silva</td>
                                    <td>03/09/2018</td>
                                    <td>1115123131</td>
                                    </tr>
                                    <tr>
                                    <td>2</td>
                                    <td>João J</td>
                                    <td>02/08/2018</td>
                                    <td>3141241241</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- fim row --> 
    </section>
    <a data-toggle="modal" data-target="#exampleModal">
        <span class="venda fas fa-cart-arrow-down" data-toggle="tooltip" data-placement="left" title="Adicionar venda"></span>
        </a>
    
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="cadVenda" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="cadVenda">Cadastrar venda</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="NomeServProd">Serviço/Produto</label>
                                    <input type="text" class="form-control" id="NomeServProd" placeholder="Nome do produto ou serviço">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="Quant">Quant.</label>
                                    <input type="text" class="form-control" id="Quant" placeholder="Quantidade">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="ValorUnit">Valor Unit.</label>
                                    <input type="text" class="form-control" id="ValorUnit" placeholder="Valor Unitário">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="ValorTotal">Total:</label>
                                    <input type="text" class="form-control" id="ValorTotal" placeholder="Total:">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <a href="vendas.jsp" class="btn btn-primary">Pagina de vendas</a>
                            <input type="submit" class="btn btn-primary" value="Cadastrar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
<jsp:include page="footer.jsp"/>