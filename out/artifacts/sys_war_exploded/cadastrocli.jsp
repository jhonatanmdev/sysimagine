<%@ page import="br.com.sysimagine.dao.DaoCliente" %>
<%@ page import="br.com.sysimagine.entidades.Cliente" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<jsp:include page="header.jsp"/>

    <section class="container">
        <form action="ServletCliente" method="post" class="my-3">
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="ID">ID</label>
                    <input type="text" class="form-control" id="ID" name="id" placeholder="ID" required value="${id.id}" readonly="readonly">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputNome">Nome</label>
                    <input type="text" class="form-control" id="inputNome" name="nomeCliente" placeholder="Nome" required value="${id.nome}">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputCpf">CPF</label>
                    <input type="text" class="form-control" id="inputCpf" name="cpfCliente" placeholder="CPF" value="${id.cpf}">
                    <small id="cpfHelp" class="form-text text-muted">${cpferror}</small>
                </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputTel">Celular</label>
                    <input type="tel" class="form-control" id="inputTel" name="telClienteUm" placeholder="Celular" required value="${id.celularUm}">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputTel2">Celular 2</label>
                    <input type="tel" class="form-control" id="inputTel2" name="telClienteDois" placeholder="Celular 2" value="${id.celularDois}">
                </div>
            </div>
            
            <button type="submit" class="btn btn-primary float-right">Salvar</button>
        </form>
        <div class="row my-5">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-header alert-success">
                        <h5 class="card-header-text float-left">Clientes cadastrados</h5>
                        <form class="col-md-5 float-right">
                            <div class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-addon">
                             <button class="fa fa-search btnsearch" type="submit"></button>
                            </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-uppercase">
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Data de Cad.</th>
                                        <th>CPF/CNPJ</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <%
                                    DaoCliente daoCliente = new DaoCliente();
                                    List<Cliente> lista = daoCliente.listar();
                                    for (Cliente c: lista){

                                %>

                                <tr>
                                    <td><%= c.getId() %></td>
                                    <td><%= c.getNome() %></td>
                                    <td><%= c.getData_cadastro() %></td>
                                    <td><%= c.getCpf() %></td>
                                    <td>
                                        <a href="ServletCliente?acao=editar&id=<%= c.getId() %>"><i class="fas fa-user-edit"></i></a>
                                        <a href="ServletCliente?acao=deletar&id=<%= c.getId() %>" class="text-danger"><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>

                                <% } %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- fim row -->
    </section>
<jsp:include page="footer.jsp"/>