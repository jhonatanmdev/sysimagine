<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="header.jsp"/>
    <section class="container">
        <div class="row">
            <div class="col-md-12 my-1">
                <div class="card ">
                    <div class="card-header alert-warning">
                        <h5 class="card-header-text">Vendas simples</h5>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-uppercase">
                                        <th>ID</th>
                                        <th>Serviço</th>
                                        <th>Data</th>
                                        <th>Quantidade</th>
                                        <th>Valor</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tfoot class="alert-warning">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>Total:</th>
                                        <th>R$ 3,25</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Xerox PB</td>
                                        <td>17/10/2018</td>
                                        <td>5</td>
                                        <td>0,25</td>
                                        <td>1,25</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Boleto</td>
                                        <td>17/10/2018</td>
                                        <td>1</td>
                                        <td>2,00</td>
                                        <td>2,00</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div><!-- fim row -->
    </section>

    <a data-toggle="modal" data-target="#exampleModal">
        <span class="venda fas fa-cart-arrow-down" data-toggle="tooltip" data-placement="left" title="Adicionar venda"></span>
    </a>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="cadVenda" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="cadVenda">Cadastrar venda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    <div class="modal-body">

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="NomeServProd">Serviço/Produto</label>
                                <input type="text" class="form-control" id="NomeServProd" placeholder="Nome do produto ou serviço">
                            </div>
                            
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="Quant">Quant.</label>
                                <input type="text" class="form-control" id="Quant" placeholder="Quantidade">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ValorUnit">Valor Unit.</label>
                                <input type="text" class="form-control" id="ValorUnit" placeholder="Valor Unitário">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ValorTotal">Total:</label>
                                <input type="text" class="form-control" id="ValorTotal" placeholder="Total:">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <a href="vendas.jsp" class="btn btn-primary">Pagina de vendas</a>
                        <input type="submit" class="btn btn-primary" value="Cadastrar">
                    </div>
                </form>
            </div>
        </div>
    </div>
<jsp:include page="footer.jsp"/>