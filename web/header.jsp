<%--
  Created by IntelliJ IDEA.
  User: Lenk
  Date: 13/11/2018
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html lang="pt-br">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/compiler/bootstrap.css">
    <link rel="stylesheet" href="node_modules/bootstrap/compiler/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/solid.css" integrity="sha384-VGP9aw4WtGH/uPAOseYxZ+Vz/vaTb1ehm1bwx92Fm8dTrE+3boLfF1SpAtB1z7HW"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/fontawesome.css" integrity="sha384-1rquJLNOM3ijoueaaeS5m+McXPJCGdr5HcA03/VHXxcp2kX2sUrQDmFc3jR5i/C7"
          crossorigin="anonymous">
    <title>SysImagine</title>
</head>

<body>
<header>
    <nav class="navbar navbar-fixed-top navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
            <a class="navbar-brand h1 mb-0" href="#">SysImagine</a>

            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarSite">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSite">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link " href="dashboard.jsp">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="vendas.jsp">Vendas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="pedidos.jsp">Pedidos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="relatorio.jsp">Relatório</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDropCadastro">
                            Cadastros
                        </a>
                        <div class="dropdown-menu ">
                            <a class="dropdown-item" href="cadastrocli.jsp">Clientes</a>
                            <a class="dropdown-item" href="cadastroencom.jsp">Encomendas</a>
                            <a class="dropdown-item" href="cadastroprodserv.jsp">Serviços simples</a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline">
                    <button class="btn btn-dark" type="submit">Sair</button>
                </form>
            </div> <!-- fim nav-->
        </div> <!-- fim container -->
    </nav>
</header>
