<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>SysImagine</title>
    <meta charset="utf-8">
      <link rel="stylesheet" href="node_modules/bootstrap/compiler/bootstrap.css">
  </head>
  <body>
      <section class="container col-md-6 col-lg-3 my-5">  
        <h1>SysImagine</h1>
        <form>
          <div class="form-group my-4 ">
            <input type="text" class="form-control" id="usuemail" placeholder="Código do pedido">
            <small class="form-text text-muted">Consulte o status do seu pedido</small>
          </div>
            
          <button type="submit" class="btn btn-primary float-right">Consultar</button>
        </form>
      </section>

      <jsp:include page="footer.jsp"/>
