<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="header.jsp"/>
    <section class="container">
        <form class="my-3">
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="inputTipo">Tipo</label>
                    <select class="form-control" id="inputTipo">
                        <option value="produto">Produto</option>
                        <option value="servico">Serviço</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputNomeEnc">Encomenda</label>
                    <input type="text" class="form-control" id="inputNomeEnc" placeholder="Nome do produto ou serviço">
                </div>
                <div class="form-group col-md-2">
                    <label for="inputTam">Tamanho</label>
                    <input type="text" class="form-control" id="inputTam" placeholder="Tamanho">
                </div>
                <div class="form-group col-md-2">
                    <label for="unidadeTipo">Unidade</label>
                    <select class="form-control" id="unidadeTipo">
                        <option value="m">Metro</option>
                        <option value="cm">Centimetro</option>
                    </select>
                </div>
                
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDesc">Descrição</label>
                    <input type="text" class="form-control" id="inputDesc" placeholder="Descrição">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputCli">Cliente</label>
                    <input type="tel" class="form-control" id="inputCli" placeholder="Cliente">
                </div>
                <div class="form-group col-md-2">
                        <label for="inputData">Data de Entrega</label>
                        <input type="date" class="form-control" id="inputData">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="inputQuant">Quantidade</label>
                    <input type="number" class="form-control" id="inputQuant" placeholder="Quantidade">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputUnit">Valor Unitário</label>
                    <input type="text" class="form-control" id="inputUnit" placeholder="Valor unitário">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputTot">Valor Total</label>
                    <input type="text" class="form-control" id="inputTot" placeholder="Valor total">
                </div>
                <div class="form-group col-md-2">
                        <label for="inputStatus">Status</label>
                        <select class="form-control" id="inputStatus">
                            <option value="entregue">Entregue</option>
                            <option value="atraso">Atraso</option>
                            <option value="emprodução">Em produção</option>
                            <option value="cancelado">Cancelado</option>
                        </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary float-right">Salvar</button>
        </form>
        <div class="row my-5">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-header alert-success">
                            <h5 class="card-header-text float-left">Encomendas cadastrados</h5>
                            <form class="busca float-right">
                                <div class="input-group">
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon">
                                 <button class="fa fa-search btnsearch" type="submit"></button>
                                </span>
                                </div>
                            </form>
                            </div>                                     
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-uppercase">
                                        <th>ID</th>
                                        <th>Encomenda</th>
                                        <th>Tipo</th>
                                        <th>Qnt.</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Banner 4x2m</td>
                                        <td>Produto</td>
                                        <td>1</td>
                                        <td>360,00</td>
                                        <td><span class="badge badge-success">Entregue</span></td>
                                        <td>
                                            <a href="#"><i class="fas fa-user-edit"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Arte Gráfica</td>
                                        <td>Serviço</td>
                                        <td>1</td>
                                        <td>80,00</td>
                                        <td><span class="badge badge-warning">Em produção</span></td>
                                        <td>
                                            <a href="#"><i class="fas fa-user-edit"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>


    </section>

<jsp:include page="footer.jsp"/>