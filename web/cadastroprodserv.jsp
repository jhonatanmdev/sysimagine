<jsp:include page="header.jsp"/>
    <section class="container">
        <form class="my-3">
            <div class="form-row">
                <div class="form-group col-md-8">
                    <label for="NomeServProd">Serviço/Produto</label>
                    <input type="text" class="form-control" id="NomeServProd" placeholder="Nome do produto ou serviço">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputCpf">Valor Unit.</label>
                    <input type="text" class="form-control" id="inputCpf" placeholder="Valor Unitário">
                </div>
            </div>
            <button type="submit" class="btn btn-primary float-right">Cadastrar</button>
        </form>
        <div class="row my-5">
            <div class="col-md-12 ">
                <div class="card">
                    <div class="card-header alert-success">
                        <h5 class="card-header-text float-left">Clientes cadastrados</h5>
                        <form class="col-md-5 float-right">
                            <div class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-addon">
                             <button class="fa fa-search btnsearch" type="submit"></button>
                            </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-block">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-uppercase">
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Valor Unitário</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Xerox PB</td>
                                        <td>R$ 0,25</td>
                                        <td>
                                            <a href="#"><i class="fas fa-user-edit"></i></a>
                                            <a href="#" class="text-danger"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Xerox Col</td>
                                        <td>R$ 1,00</td>
                                        <td>
                                            <a href="#"><i class="fas fa-user-edit"></i></a>
                                            <a href="#" class="text-danger"><i class="fas fa-trash-alt "></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- fim row -->
    </section>


<jsp:include page="footer.jsp"/>