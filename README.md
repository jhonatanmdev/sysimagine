**SysImagine - Sistema Para Gráfica Rápida**

Atualmente sou sócio em uma gráfica pequena, então foi pensado em um sistema simples e rápido, baseado em nossas necessidades. Onde temos botão de ação rápida para vendas, sem a necessidade de preenchimento de dados de cliente para vendas commo xerox, impressão, dvd's, cd's e serviços mais simples.

*O objetivo é facilitar mais ainda a venda, em um sistema simples e ideal.*

---

## Telas

Segue as telas e a descrição.

1. **Index** pagina inicial de login com botão de **Entrar** para acesso direto.
2. **Dashboard** pagina onde teremos uma vista geral do negócio, desde pedidos a entregar até clientes cadastrados.
3. **Vendas** teremos acesso as vendas realizadas.
4. **Pedidos** página com visão geral dos pedidos, inclusive com a possibilidade de fazer venda rápida.
5. **Relatório** página de relatório, poderá ser feito um relatório das vendas realizadas em determinado período *[em construção - em análise]*.
6. **Consultar** agora a consulta poderá ser feita na barra de pesquisa na parte de cadastro.
7. **Cadastros** poderá ser feito cadastro ou consulta de cliente, encomendas ou serviços simples.
8. **Sair** deslogar do sistema.
9. **consultar.html** o cliente poderá consultar o status do pedido sem a necessidade de login 
10. **Botão de adicionar venda** Teremos uma moda para cadastrar vendas simples, como xerox, impressão etc, estas que não necessitam de acompanhamento do cliente ou cadastro.


---

*Joanantha Matheus - COMP4N*
*joananthadesign@gmail.com*