package br.com.sysimagine.entidades;

import java.util.Date;

public class Cliente {
    private String id;
    private String nome;
    private String cpf;
    private String celularUm;
    private String celularDois;
    private Date data_cadastro;

    public Cliente() {
    }

    public Cliente(String nome, String cpf, String celularUm, String celularDois) {
        this.nome = nome;
        this.cpf = cpf;
        this.celularUm = celularUm;
        this.celularDois = celularDois;
    }

    public Date getData_cadastro() {
        return data_cadastro;
    }

    public void setData_cadastro(Date data_cadastro) {
        this.data_cadastro = data_cadastro;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCelularUm() {
        return celularUm;
    }

    public void setCelularUm(String celularUm) {
        this.celularUm = celularUm;
    }

    public String getCelularDois() {
        return celularDois;
    }

    public void setCelularDois(String celularDois) {
        this.celularDois = celularDois;
    }
}
