package br.com.sysimagine.servlet;

import br.com.sysimagine.dao.DaoCliente;
import br.com.sysimagine.entidades.Cliente;
import br.com.sysimagine.util.CPF;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/ServletCliente"}, name = "ServletCliente")
public class ServletCliente extends HttpServlet {
    private DaoCliente daoCliente = new DaoCliente();

    List<Cliente> lista = daoCliente.listar();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String nome = request.getParameter("nomeCliente");
        String cpf = request.getParameter("cpfCliente");
        String celularUm = request.getParameter("telClienteUm");
        String celularDois = request.getParameter("telClienteDois");

        Cliente cliente = new Cliente();
        CPF cpfvalida = new CPF(cpf);
        if(cpf.isEmpty()){
            cliente.setCpf(cpf);
        }else if(cpfvalida.isCPF()){
            cliente.setCpf(cpfvalida.getCPF(false));
        }else{
            response.setContentType("text/html;charset=UTF-8");
            request.setAttribute("cpferror", "CPF Inválido.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrocli.jsp");
            dispatcher.forward(request, response);
            return;
        }
        cliente.setId(id);
        cliente.setNome(nome);
        cliente.setCelularUm(celularUm);
        cliente.setCelularDois(celularDois);

            if(id == null || id.isEmpty()){
                daoCliente.salvar(cliente);
            }else{
                daoCliente.atualizar(cliente);
            }
            RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrocli.jsp");
            request.setAttribute("lista", lista);
            dispatcher.forward(request, response);



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String acao = request.getParameter("acao");
        String id = request.getParameter("id");

        if(acao.equalsIgnoreCase("deletar")){
            daoCliente.delete(id);
            RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrocli.jsp");
            request.setAttribute("lista", lista);
            dispatcher.forward(request, response);
        } else if(acao.equalsIgnoreCase("editar")){
            Cliente cliente = daoCliente.consultar(id);
            RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrocli.jsp");
            request.setAttribute("id", cliente);
            dispatcher.forward(request, response);
        }else{
            RequestDispatcher dispatcher = request.getRequestDispatcher("cadastrocli.jsp");
            request.setAttribute("lista", lista);
            dispatcher.forward(request, response);
        }
    }
}
