package br.com.sysimagine.servlet;

import br.com.sysimagine.dao.Dao;
import br.com.sysimagine.entidades.Administrador;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/ServletLogin"}, name = "ServletLogin")
public class ServletLogin extends HttpServlet {

    private Dao DaoLogin = new Dao();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");
        try {
            if (DaoLogin.validarLogin(login, senha)) {
                Administrador admin = new Administrador();
                admin.setUser(login);
                admin.setSenha(senha);

                HttpSession session = request.getSession();
                session.setAttribute("admin", admin);

                RequestDispatcher dispatcher = request.getRequestDispatcher("dashboard.jsp");
                dispatcher.forward(request, response);
            } else {
                response.setContentType("text/html;charset=UTF-8");
                request.setAttribute("error", "Login ou senha inválidos.");
                RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
                dispatcher.forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
