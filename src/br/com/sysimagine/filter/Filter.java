package br.com.sysimagine.filter;

import br.com.sysimagine.connection.Conexao;
import br.com.sysimagine.entidades.Administrador;
import br.com.sysimagine.entidades.Cliente;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebFilter(urlPatterns = {"/cadastrocli.jsp", "/header.jsp", "/footer.jsp", "/cadastroencom.jsp", "/cadastroprodserv.jsp", "/consultar.jsp", "/dashboard.jsp", "/pedidos.jsp", "/relatorio.jsp", "/vendas.jsp"}, filterName = "Filter")
public class Filter implements javax.servlet.Filter {
    private static Connection connection;
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpSession session = request.getSession();
        Administrador admin = (Administrador) session.getAttribute("admin");

        if(admin == null){
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(req, resp);
            return;
        }

        try {
            chain.doFilter(req, resp);
            connection.commit();
        }catch (Exception e ){
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        }
    }

    public void init(FilterConfig config) throws ServletException {
        connection = Conexao.getConexao();
    }

}
