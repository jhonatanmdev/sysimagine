package br.com.sysimagine.dao;

import br.com.sysimagine.connection.Conexao;
import br.com.sysimagine.entidades.Cliente;
import com.mysql.cj.protocol.Resultset;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DaoCliente {
    private Connection connection;

    public DaoCliente(){
        connection = Conexao.getConexao();
    }

    public void salvar(Cliente cliente)  {
        try {
            String sql = "insert into cliente(nome_cli, cpf_cli, cel_cli, cel_dois_cli, data_cad, usuario_id_user) values (?, ?, ?, ?, now(), 1)";
            PreparedStatement insert = connection.prepareStatement(sql);
            insert.setString(1, cliente.getNome());
            insert.setString(2, cliente.getCpf());
            insert.setString(3, cliente.getCelularUm());
            insert.setString(4, cliente.getCelularDois());
            insert.execute();
            connection.commit();
        }catch (SQLException e) {
            e.printStackTrace();

            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

    }
    public List<Cliente> listar(){
        List<Cliente> clienteList = new ArrayList<>();
        String sql = "select * from cliente";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Cliente cliente = new Cliente();
                cliente.setId(resultSet.getString("id_cli"));
                cliente.setNome(resultSet.getString("nome_cli"));
                cliente.setData_cadastro(resultSet.getDate("data_cad"));
                cliente.setCpf(resultSet.getString("cpf_cli"));
                clienteList.add(cliente);
            }
            resultSet.close();
            } catch (SQLException e) {
            e.printStackTrace();
        }
        return clienteList;
    }

    public void delete(String id){
        String sql = "delete from cliente where id_cli='"+id+"'";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        }
    }

    public Cliente consultar(String id) {
        String sql = "select * from cliente where id_cli ='"+id+"'";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultset = preparedStatement.executeQuery();
            if(resultset.next()){
                Cliente cliente = new Cliente();
                cliente.setId(resultset.getString("id_cli"));
                cliente.setNome(resultset.getString("nome_cli"));
                cliente.setCpf(resultset.getString("cpf_cli"));
                cliente.setCelularUm(resultset.getString("cel_cli"));
                cliente.setCelularDois(resultset.getString("cel_dois_cli"));
                return cliente;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void atualizar(Cliente cliente) {
        String sql = "update cliente set nome_cli = ?, cpf_cli = ?, cel_cli = ?, cel_dois_cli = ? where id_cli = "+cliente.getId();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, cliente.getNome());
            preparedStatement.setString(2, cliente.getCpf());
            preparedStatement.setString(3, cliente.getCelularUm());
            preparedStatement.setString(4, cliente.getCelularDois());
            preparedStatement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

        }
    }
}
