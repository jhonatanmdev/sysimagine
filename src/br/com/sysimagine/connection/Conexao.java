package br.com.sysimagine.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {
    public static Connection getConexao(){
        String driver = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/sysimagine?useTimezone=true&serverTimezone=UTC";
        String user = "root";
        String password = "";
        try{

            Class.forName(driver).newInstance();

            Connection con = DriverManager.getConnection(url, user, password);
            con.setAutoCommit(false);
            return con;
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void fecharConexao(Connection connection){
        try {
            if(connection != null){
                connection.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
